module gitee.com/fishinthefountain/xorm_db

go 1.16

require (
	xorm.io/builder v0.3.9
	xorm.io/core v0.7.3
	xorm.io/xorm v1.0.7
)
