package xorm_db

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"time"
	"xorm.io/core"
	"xorm.io/xorm"
)

// 默认数据库链接
var stdEngine *xorm.Engine

// 初始化控制
var intiOnce sync.Once

// Init 初始化默认链接
func Init(conf IDBConf) (err error) {
	// 只执行一次
	intiOnce.Do(func() {
		stdEngine, err = Connect(conf)
	})

	return
}

// Connect 链接数据库
func Connect(conf IDBConf) (engine *xorm.Engine, err error) {
	// 创建数据库引擎
	if engine, err = xorm.NewEngine(conf.GetType(), conf.GetDSN()); nil != err {
		return
	}

	// 测试连接
	if err = engine.Ping(); nil != err {
		return
	}

	// 调试模式输出SQL语句
	engine.ShowSQL(conf.GetDebug())
	// 设置最大链接数
	engine.SetMaxOpenConns(conf.GetMaxOpenConnect())
	// 设置最大闲置连接数
	engine.SetMaxIdleConns(conf.GetMaxIdleConnect())
	// 设置链接最大时间
	engine.SetConnMaxLifetime(time.Second * 12 * 60 * 60)
	// 设置表前缀
	tbMapper := core.NewPrefixMapper(core.SnakeMapper{}, conf.GetTablePrefix())
	engine.SetTableMapper(tbMapper)

	return
}

// Engine 获取默认链接
func Engine() *xorm.Engine {
	return stdEngine
}
