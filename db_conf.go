package xorm_db

// IDBConf DB配置接口
type IDBConf interface {
	// GetDebug 是否开启调试
	GetDebug() bool
	// GetDSN DSN
	GetDSN() string
	// GetType DB类型
	GetType() string
	// GetConnectMaxLifeTime 链接最大生存时间
	GetConnectMaxLifeTime() int
	// GetMaxOpenConnect 最大开放链接数
	GetMaxOpenConnect() int
	// GetMaxIdleConnect 最大闲置链接数
	GetMaxIdleConnect() int
	// GetTablePrefix 表前缀
	GetTablePrefix() string
}

// DefaultConf 默认DB配置实现
type DefaultConf struct {
	Debug              bool
	DSN                string
	Type               string
	ConnectMaxLifeTime int
	MaxOpenConnect     int
	MaxIdleConnect     int
	TablePrefix        string
}

// GetDebug 是否开启调试
func (c DefaultConf) GetDebug() bool {
	return c.Debug
}

// GetDSN DSN
func (c DefaultConf) GetDSN() string {
	return c.DSN
}

// GetType DB类型
func (c DefaultConf) GetType() string {
	return c.Type
}

// GetConnectMaxLifeTime 链接最大生存时间
func (c DefaultConf) GetConnectMaxLifeTime() int {
	return c.ConnectMaxLifeTime
}

// GetMaxOpenConnect 最大开放链接数
func (c DefaultConf) GetMaxOpenConnect() int {
	return c.MaxOpenConnect
}

// GetMaxIdleConnect 最大闲置链接数
func (c DefaultConf) GetMaxIdleConnect() int {
	return c.MaxIdleConnect
}

// GetTablePrefix 表前缀
func (c DefaultConf) GetTablePrefix() string {
	return c.TablePrefix
}
