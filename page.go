package xorm_db

// IPage 分页接口
type IPage interface {
	// GetPage 当前页码
	GetPage() int
	// GetPageSize 每页记录数
	GetPageSize() int
	// GetLimit SQL LIMIT
	GetLimit() int
	// GetOffset SQL OFFSET
	GetOffset() int
}

// Page 分页接口默认实现
type Page struct {
	Page     int
	PageSize int
}

// GetPage 当前页码
func (p Page) GetPage() int {
	return p.Page
}

// GetPageSize 每页记录数
func (p Page) GetPageSize() int {
	return p.PageSize
}

// GetLimit SQL LIMIT
func (p Page) GetLimit() int {
	return p.PageSize
}

// GetOffset SQL OFFSET
func (p Page) GetOffset() int {
	return (p.Page - 1) * p.PageSize
}

// NewPage 获取新的Page实例
func NewPage(page, pageSize int) *Page {
	p := &Page{
		Page:     page,
		PageSize: pageSize,
	}

	return p
}
